const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const clean = require("gulp-clean");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const shorthand = require("gulp-shorthand");
const rename = require("gulp-rename");
const jsminify = require("gulp-uglify");
const cssminify = require("gulp-csso");
const babel = require("gulp-babel");
// const imgmin = require("gulp-imagemin");

function server() {
    return browserSync.init({
        server: {
            baseDir: "./",
        },
    });
}

//----------Виделення директорій
function clear() {
    return gulp.src("./dist/*", { read: false }).pipe(clean());
}
exports.clear = clear;


//----------Обробка CSS
function buildStyles() {
    return gulp
        .src("./src/scss/**/main.scss")
        .pipe(
            plumber({
                errorHandler: notify.onError((error) => ({
                    title: "SCSS",
                    message: error.message,
                })),
            })
        )
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(autoprefixer())
        .pipe(shorthand())
        .pipe(gulp.dest("./dist/"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(cssminify())
        .pipe(gulp.dest("./dist/"));
}
exports.buildStyles = buildStyles;
function watchStyles() {
    return gulp
        .watch("./src/scss/**/*.scss", buildStyles)
        .on("change", browserSync.reload);
}
exports.watchStyles = watchStyles;


//----------Обробка JS
function buildScripts() {
    return gulp
        .src("./src/js/**/*.js")
        .pipe(
            plumber({
                errorHandler: notify.onError((error) => ({
                    title: "JS",
                    message: error.message,
                })),
            })
        )
        // .pipe(babel({
        //     presets: ['@babel/env']
        // }))
        .pipe(concat("script.js"))
        .pipe(gulp.dest("./dist/"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(jsminify())
        .pipe(gulp.dest("./dist/"));
}
exports.buildScripts = buildScripts;
function watchScripts() {
    return gulp
        .watch("./src/js/**/*.js", buildScripts)
        .on("change", browserSync.reload);
}
exports.watchScripts = watchScripts;
//----------Обробка зображень
function img() {
    return gulp
        .src("./src/img/**/*.{png,jpg,jpeg,gif,svg,ico}")
        .pipe(
            plumber({
                errorHandler: notify.onError((error) => ({
                    title: "IMG",
                    message: error.message,
                })),
            })
        )
        // .pipe(imgmin({
        //     verbose: true
        // }))
        .pipe(gulp.dest("./dist/img"));
}
exports.img = img;
function watchImg() {
    return gulp
        .watch("./src/img/**/*.{png,jpg,jpeg,gif,svg}", img)
        .on("change", browserSync.reload);
}
exports.watchImg = watchImg;
//----------Фінальні задачі gulp
exports.dev =
    gulp.parallel(watchStyles, watchScripts, server);
exports.build = gulp.series(
    clear,
    gulp.parallel(buildStyles, buildScripts, img));